package util

import (
	"log"
	"strconv"

	jsoniter "github.com/json-iterator/go"
	"google.golang.org/grpc"
)

const (
	DefaultPage = 1
)

var Json = jsoniter.ConfigCompatibleWithStandardLibrary

func Dial(addr string) *grpc.ClientConn {
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		log.Fatal("could not connect to", addr, err)
	}
	return conn
}

func StringToInt(s string) int {
	i, _ := strconv.Atoi(s)
	return i
}
