package main

import (
	"fmt"
	"movie-gateway/route"
	"os"

	"github.com/joho/godotenv"
	"github.com/qasir-id/qicore/util"
	logger "github.com/sirupsen/logrus"
)

func main() {
	// load ev
	err := godotenv.Load()
	if err != nil {
		logger.Fatal("Error loading .env file")
	}

	// connect route api
	e := route.Init()
	data, err := util.Json.MarshalIndent(e.Routes(), "", "  ")
	if err != nil {
		panic(fmt.Sprint(err))
	}
	fmt.Println(string(data))

	e.Logger.Fatal(e.Start(":" + os.Getenv("PORT")))
}
