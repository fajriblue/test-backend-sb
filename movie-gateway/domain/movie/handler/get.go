package handler

import (
	"movie-gateway/domain/movie/client"
	pb "movie-gateway/proto/movie"
	"movie-gateway/util"
	"net/http"

	"github.com/labstack/echo/v4"
)

type Get struct {
	iClient client.IClient
}

func NewGet() *Get {
	return &Get{
		iClient: client.NewClient(),
	}
}

func (h *Get) Handle(c echo.Context) (err error) {
	ctx := c.Request().Context()
	// hit grpc Get
	req := &pb.GetRequest{
		ImdbID: c.Param("ImdbID"),
	}
	response, err := h.iClient.Get(ctx, req)
	if err != nil {
		return
	}
	// structure response
	resp := &util.Response{
		Code:    http.StatusOK,
		Message: http.StatusText(http.StatusOK),
		Data: map[string]interface{}{
			"movie": response.GetMovie(),
		},
	}
	// response for json
	return resp.JSON(c)
}
