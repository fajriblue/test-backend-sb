package handler

import (
	"movie-gateway/domain/movie/client"
	pb "movie-gateway/proto/movie"
	"movie-gateway/util"
	"net/http"

	"github.com/labstack/echo/v4"
)

type List struct {
	iClient client.IClient
}

func NewList() *List {
	return &List{
		iClient: client.NewClient(),
	}
}

func (h *List) Handle(c echo.Context) (err error) {
	ctx := c.Request().Context()
	// hit grpc list
	page := util.StringToInt(c.QueryParam("pagination"))
	if page == 0 {
		page = util.DefaultPage
	}
	req := &pb.ListRequest{
		Search: c.QueryParam("searchword"),
		Page:   int32(page),
	}
	response, err := h.iClient.List(ctx, req)
	if err != nil {
		return
	}
	// structure response
	resp := &util.Response{
		Code:    http.StatusOK,
		Message: http.StatusText(http.StatusOK),
		Data: map[string]interface{}{
			"movies": response.Search,
		},
	}
	// convert pagination
	var pg util.Pagination
	bytes, err := util.Json.Marshal(&response.Pagination)
	util.Json.Unmarshal(bytes, &pg)
	resp = resp.WithPagination(c, pg)
	// response for json
	return resp.JSON(c)
}
