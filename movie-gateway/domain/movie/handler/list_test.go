package handler

import (
	"movie-gateway/domain/movie/client/mocks"
	pb "movie-gateway/proto/movie"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"
	"testing"

	logger "github.com/sirupsen/logrus"

	"github.com/bxcodec/faker"

	"github.com/stretchr/testify/suite"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

type ListTestSuite struct {
	suite.Suite
	cRepo *mocks.IClient
	act   *List
}

func (s *ListTestSuite) SetupTest() {
	s.cRepo = new(mocks.IClient)
	s.act = NewList()
	s.act.iClient = s.cRepo
}

func TestListTestSuite(t *testing.T) {
	suite.Run(t, new(ListTestSuite))
}

func (s *ListTestSuite) AfterTest(_, _ string) {
	s.cRepo.AssertExpectations(s.T())
}

func (s *ListTestSuite) TestList_Handle() {
	e := echo.New()

	q := make(url.Values)
	q.Set("searchword", "Batman")
	q.Set("pagination", "1")

	req := httptest.NewRequest(http.MethodGet, "/?"+q.Encode(), nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/api/movies")

	ctx := c.Request().Context()

	i, _ := strconv.ParseInt(c.QueryParam("pagination"), 10, 8)
	logger.Info(i)
	request := pb.ListRequest{
		Search: c.QueryParam("searchword"),
		Page:   int32(i),
	}

	var result *pb.ListResponse
	err := faker.FakeData(&result)

	s.cRepo.On("List", ctx, &request).Return(result, nil)

	err = s.act.Handle(c)
	assert.Nil(s.T(), err)
	assert.Equal(s.T(), http.StatusOK, rec.Code)
}
