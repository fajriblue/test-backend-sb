package handler

import (
	"movie-gateway/domain/movie/client/mocks"
	pb "movie-gateway/proto/movie"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/bxcodec/faker"

	"github.com/stretchr/testify/suite"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

type GetTestSuite struct {
	suite.Suite
	cRepo *mocks.IClient
	act   *Get
}

func (s *GetTestSuite) SetupTest() {
	s.cRepo = new(mocks.IClient)
	s.act = NewGet()
	s.act.iClient = s.cRepo
}

func TestGetTestSuite(t *testing.T) {
	suite.Run(t, new(GetTestSuite))
}

func (s *GetTestSuite) AfterTest(_, _ string) {
	s.cRepo.AssertExpectations(s.T())
}

func (s *GetTestSuite) TestGet_Handle() {
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/api/movies/:ImdbID")
	c.SetParamNames("ImdbID")
	c.SetParamValues("tt4853102")

	ctx := c.Request().Context()
	request := pb.GetRequest{
		ImdbID: c.Param("ImdbID"),
	}

	var result *pb.GetResponse
	err := faker.FakeData(&result)

	s.cRepo.On("Get", ctx, &request).Return(result, nil)

	err = s.act.Handle(c)
	assert.Nil(s.T(), err)
	assert.Equal(s.T(), http.StatusOK, rec.Code)
}
