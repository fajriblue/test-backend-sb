package client

import (
	"context"
	pb "movie-gateway/proto/movie"
	"movie-gateway/util"
	"os"
)

type IClient interface {
	List(context.Context, *pb.ListRequest) (*pb.ListResponse, error)
	Get(context.Context, *pb.GetRequest) (*pb.GetResponse, error)
}

type Client struct {
}

func NewClient() *Client {
	return &Client{}
}

func (c *Client) List(ctx context.Context, request *pb.ListRequest) (*pb.ListResponse, error) {
	conn := util.Dial(os.Getenv("MOVIE_SERVICE"))
	defer conn.Close()
	client := pb.NewMovieServiceClient(conn)
	return client.List(ctx, request)
}

func (c *Client) Get(ctx context.Context, request *pb.GetRequest) (*pb.GetResponse, error) {
	conn := util.Dial(os.Getenv("MOVIE_SERVICE"))
	defer conn.Close()
	client := pb.NewMovieServiceClient(conn)
	return client.Get(ctx, request)
}
