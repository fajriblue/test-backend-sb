package route

import (
	movie "movie-gateway/domain/movie/handler"

	"github.com/labstack/echo/v4"
)

// Handler endpoint to use it later
type Handler interface {
	Handle(c echo.Context) (err error)
}

var endpoint = map[string]Handler{
	//movie
	"list_movie": movie.NewList(),
	"get_movie":  movie.NewGet(),
}
