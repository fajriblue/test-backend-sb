module movie-gateway

go 1.16

require (
	github.com/bxcodec/faker v2.0.1+incompatible
	github.com/go-playground/locales v0.14.0
	github.com/go-playground/universal-translator v0.18.0
	github.com/joho/godotenv v1.4.0
	github.com/json-iterator/go v1.1.12
	github.com/labstack/echo/v4 v4.6.1
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/qasir-id/qicore v0.0.2
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	google.golang.org/grpc v1.43.0
	google.golang.org/protobuf v1.25.0
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0
)
