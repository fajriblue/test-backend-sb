## movie-gateway

### Structure
```
├── domain
├── proto
├── route
├── util             
├── main.go
```
- Domain: package for handling request, hit service use grpc, build response
- Proto: folder to collect protobuf file
- Route: configuration of route
- Util: common app process / helper

### Testing
How to run unit test :
- go test -cover -coverprofile=coverage.out ./...

check coverage result :
- go tool cover -html=coverage.out