package errors

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	InvalidArgument codes.Code = 400
)

func ErrorInvalid(msg string) error {
	return status.Errorf(InvalidArgument, msg)
}
