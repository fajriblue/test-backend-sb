package util

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStringToInt32(t *testing.T) {
	s := "12"
	res := StringToInt32(s)
	assert.Equal(t, int32(12), res)
}
