package util

import (
	"strconv"

	jsoniter "github.com/json-iterator/go"
)

const (
	PageSize = 10
)

var Json = jsoniter.ConfigCompatibleWithStandardLibrary

func StringToInt32(s string) int32 {
	i, _ := strconv.ParseInt(s, 10, 8)
	return int32(i)
}
