package builder

import (
	pb "movie-service/entity/movie"
	"movie-service/repo/rest"
	"movie-service/util"

	"github.com/jinzhu/copier"
)

type Grpc struct {
}

// NewGrpc for initiate builder func
func NewGrpc() *Grpc {
	return &Grpc{}
}

// BuildPagination is a function build pagination grpc
func (g *Grpc) BuildPagination(currentPage, pageSize, totalResult int32) *pb.Pagination {
	return &pb.Pagination{
		PageSize:    pageSize,
		CurrentPage: currentPage,
		TotalPage:   totalResult / pageSize,
		TotalResult: totalResult,
	}
}

// BuildList is a function convert struct rest search to struct search grpc
func (g *Grpc) BuildList(data []rest.Search) (res []*pb.Search) {
	copier.Copy(&res, &data)
	return

}

// BuildList is a function convert struct rest get to struct get grpc
func (g *Grpc) BuildGet(data rest.Get) (res *pb.Movie) {
	util.Json.Unmarshal(data.Encode(), &res)
	return
}
