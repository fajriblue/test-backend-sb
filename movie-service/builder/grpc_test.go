package builder

import (
	pb "movie-service/entity/movie"
	"movie-service/repo/rest"
	"testing"

	"github.com/bxcodec/faker"

	"github.com/stretchr/testify/assert"
)

func TestGrpc_BuildGet(t *testing.T) {
	var reqGet rest.Get
	_ = faker.FakeData(&reqGet)

	b := NewGrpc()
	resActual := b.BuildGet(reqGet)

	assert.Equal(t, reqGet.Title, resActual.Title)
	assert.Equal(t, reqGet.ImdbID, resActual.ImdbID)
	assert.Equal(t, reqGet.Actors, resActual.Actors)
}

func TestGrpc_BuildList(t *testing.T) {
	var reqList []rest.Search
	_ = faker.FakeData(&reqList)

	b := NewGrpc()
	resActual := b.BuildList(reqList)

	assert.Equal(t, reqList[0].Title, resActual[0].Title)
	assert.Equal(t, reqList[0].ImdbID, resActual[0].ImdbID)
}

func TestGrpc_BuildPagination(t *testing.T) {
	var (
		currentPage int32 = 1
		pageSize    int32 = 10
		totalResult int32 = 100
	)
	resExpection := pb.Pagination{
		PageSize:    pageSize,
		CurrentPage: currentPage,
		TotalPage:   totalResult / pageSize,
		TotalResult: totalResult,
	}
	b := NewGrpc()
	resActual := b.BuildPagination(currentPage, pageSize, totalResult)

	assert.Equal(t, &resExpection, resActual)
}
