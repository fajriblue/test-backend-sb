module movie-service

go 1.16

require (
	github.com/bxcodec/faker v2.0.1+incompatible
	github.com/go-sql-driver/mysql v1.6.0
	github.com/golang-migrate/migrate/v4 v4.15.1
	github.com/jinzhu/copier v0.3.4
	github.com/jmoiron/sqlx v1.3.4
	github.com/joho/godotenv v1.4.0
	github.com/json-iterator/go v1.1.10
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	github.com/zhashkevych/go-sqlxmock v1.5.1
	google.golang.org/grpc v1.43.0
	google.golang.org/protobuf v1.27.1
)
