package main

import (
	"movie-service/repo/mysql"
	"movie-service/transport"

	"github.com/joho/godotenv"
	logger "github.com/sirupsen/logrus"
)

func main() {
	// load env
	err := godotenv.Load()
	if err != nil {
		logger.Fatal("Error loading .env file")
	}
	// run migration
	mysql.RunMigrate()
	// connection mysql
	mysql.InitCon()
	// run grpc
	transport.NewServer().RunGRCP()
}
