package repo

import (
	"context"
	"movie-service/repo/mocks"
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/stretchr/testify/mock"

	"github.com/stretchr/testify/suite"
)

var ctx = context.Background()

type LogTestSuite struct {
	suite.Suite
	lRepo *mocks.IModel
	act   *Log
}

func (s *LogTestSuite) SetupTest() {
	s.lRepo = new(mocks.IModel)
	s.act = NewLog()
	s.act.iMysql = s.lRepo
}

func TestLogTestSuite(t *testing.T) {
	suite.Run(t, new(LogTestSuite))
}

func (s *LogTestSuite) AfterTest(_, _ string) {
	s.lRepo.AssertExpectations(s.T())
}

func (s *LogTestSuite) TestLog_Create() {
	s.lRepo.On("Create", ctx, mock.Anything, mock.Anything).Return(nil)
	err := s.act.Create(ctx, mock.Anything, mock.Anything, mock.Anything, mock.Anything)
	assert.Nil(s.T(), err)
}
