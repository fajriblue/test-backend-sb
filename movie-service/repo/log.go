package repo

import (
	"context"
	"movie-service/repo/mysql"
	"movie-service/util"
)

type ILog interface {
	Create(context.Context, interface{}, interface{}, string, string) error
}

type Log struct {
	iMysql mysql.IModel
}

func NewLog() *Log {
	return &Log{
		iMysql: mysql.NewModel(),
	}
}

// Create is a function create log
func (l *Log) Create(ctx context.Context, reqPayload, resPayload interface{}, apiUrl, method string) (err error) {
	req, _ := util.Json.Marshal(reqPayload)
	res, _ := util.Json.Marshal(resPayload)
	payload := mysql.Log{
		RequestPayload:  string(req),
		ResponsePayload: string(res),
		ApiUrl:          apiUrl,
		Method:          method,
	}
	err = l.iMysql.Create(ctx, payload, mysql.QueryCreateLog)
	return
}
