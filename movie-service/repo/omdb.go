package repo

import (
	"context"
	"fmt"
	pb "movie-service/entity/movie"
	"movie-service/repo/rest"
	"movie-service/util/errors"
	"net/http"
	"net/url"
	"os"
)

type IOmdb interface {
	List(context.Context, *pb.ListRequest) (rest.List, error)
	Get(context.Context, *pb.GetRequest) (rest.Get, error)
}

type Omdb struct {
	iHttp rest.IHttp
	iLog  ILog
}

func NewOmdb() *Omdb {
	return &Omdb{
		iHttp: rest.NewClient(),
		iLog:  NewLog(),
	}
}

// List is a function list omdb rest api and insert log
func (i *Omdb) List(ctx context.Context, req *pb.ListRequest) (res rest.List, err error) {
	// hit rest api omdb
	payload := url.Values{}
	payload.Add("apikey", os.Getenv("OMDB_API_KEY"))
	payload.Add("s", req.Search)
	payload.Add("page", fmt.Sprint(req.Page))
	urlOmdb := os.Getenv("OMDB_URL") + "?" + payload.Encode()
	if err = i.iHttp.Request(ctx, http.MethodGet, urlOmdb, nil, &res); err != nil {
		// create log
		err = i.createLog(ctx, payload, err, urlOmdb, http.MethodGet)
		if err != nil {
			return
		}
		return
	}
	// create log
	err = i.createLog(ctx, payload, res, urlOmdb, http.MethodGet)
	if err != nil {
		return
	}

	if res.Error != "" {
		err = errors.ErrorInvalid(res.Error)
		return
	}
	return
}

// Get is a function list omdb rest api and insert log
func (i *Omdb) Get(ctx context.Context, req *pb.GetRequest) (res rest.Get, err error) {
	// hit rest api omdb
	payload := url.Values{}
	payload.Add("apikey", os.Getenv("OMDB_API_KEY"))
	payload.Add("i", req.ImdbID)
	urlOmdb := os.Getenv("OMDB_URL") + "?" + payload.Encode()
	if err = i.iHttp.Request(ctx, http.MethodGet, urlOmdb, nil, &res); err != nil {
		// create log
		err = i.createLog(ctx, payload, err, urlOmdb, http.MethodGet)
		if err != nil {
			return
		}
		return
	}
	// create log
	err = i.createLog(ctx, payload, res, urlOmdb, http.MethodGet)
	if err != nil {
		return
	}
	if res.Error != "" {
		err = errors.ErrorInvalid(res.Error)
		return
	}

	return
}

func (i *Omdb) createLog(ctx context.Context, reqPayload, resPayload interface{}, apiUrl, method string) error {
	c := make(chan error, 1)
	go func() {
		c <- i.iLog.Create(ctx, reqPayload, resPayload, apiUrl, method)
	}()
	select {
	case <-ctx.Done():
		<-c
		return ctx.Err()
	case err := <-c:
		return err
	}
}
