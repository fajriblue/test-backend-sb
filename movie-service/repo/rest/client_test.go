package rest

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

var ctx = context.Background()

func TestClient_Request(t *testing.T) {
	c := NewClient()
	var res List
	urlOmdb := "http://www.omdbapi.com/?apikey=faf7e5bb&s=Batman&page=1"
	err := c.Request(ctx, http.MethodGet, urlOmdb, nil, &res)
	assert.Nil(t, err)
}
