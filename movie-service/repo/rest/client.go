package rest

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"

	logger "github.com/sirupsen/logrus"
)

type Client struct {
}

func NewClient() *Client {
	return &Client{}
}

type IHttp interface {
	Request(context.Context, string, string, interface{}, interface{}, ...Header) error
}

// Request is a function hit api
func (c *Client) Request(ctx context.Context, method string, url string, body interface{}, res interface{}, header ...Header) (err error) {
	b, _ := json.Marshal(&body)
	req, _ := http.NewRequest(method, url, bytes.NewBuffer(b))
	for _, head := range header {
		req.Header.Set(head.Key, head.Value)
	}

	req = req.WithContext(ctx)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	_ = json.Unmarshal(bodyBytes, res)

	logger.WithContext(ctx).Infof("Response Client: %s", string(bodyBytes))
	return
}
