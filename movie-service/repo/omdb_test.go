package repo

import (
	pb "movie-service/entity/movie"
	"movie-service/repo/mocks"
	"movie-service/repo/rest"
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/stretchr/testify/mock"

	"github.com/stretchr/testify/suite"
)

type OmdbTestSuite struct {
	suite.Suite
	hRepo *mocks.IHttp
	lRepo *mocks.ILog
	act   *Omdb
}

func (s *OmdbTestSuite) SetupTest() {
	s.hRepo = new(mocks.IHttp)
	s.lRepo = new(mocks.ILog)
	s.act = NewOmdb()
	s.act.iHttp = s.hRepo
	s.act.iLog = s.lRepo
}

func TestOmdbTestSuite(t *testing.T) {
	suite.Run(t, new(OmdbTestSuite))
}

func (s *OmdbTestSuite) AfterTest(_, _ string) {
	s.hRepo.AssertExpectations(s.T())
	s.lRepo.AssertExpectations(s.T())
}

func (s *OmdbTestSuite) TestOmdb_Get() {
	req := &pb.GetRequest{
		ImdbID: "tt4853102",
	}
	var res rest.Get
	s.hRepo.On("Request", ctx, mock.Anything, mock.Anything, nil, &res).Return(nil)
	s.lRepo.On("Create", ctx, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
	resActual, err := s.act.Get(ctx, req)
	assert.Nil(s.T(), err)
	assert.Equal(s.T(), res, resActual)
}

func (s *OmdbTestSuite) TestOmdb_List() {
	req := &pb.ListRequest{
		Search: "Batman",
		Page:   1,
	}
	var res rest.List
	s.hRepo.On("Request", ctx, mock.Anything, mock.Anything, nil, &res).Return(nil)
	s.lRepo.On("Create", ctx, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

	resActual, err := s.act.List(ctx, req)
	assert.Nil(s.T(), err)
	assert.Equal(s.T(), res, resActual)
}

func (s *OmdbTestSuite) TestOmdb_createLog() {
	s.lRepo.On("Create", ctx, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
	err := s.act.createLog(ctx, mock.Anything, mock.Anything, mock.Anything, mock.Anything)
	assert.Nil(s.T(), err)
}
