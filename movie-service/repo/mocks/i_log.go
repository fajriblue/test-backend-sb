// Code generated by mockery v0.0.0-dev. DO NOT EDIT.

package mocks

import (
	context "context"

	mock "github.com/stretchr/testify/mock"
)

// ILog is an autogenerated mock type for the ILog type
type ILog struct {
	mock.Mock
}

// Create provides a mock function with given fields: _a0, _a1, _a2, _a3, _a4
func (_m *ILog) Create(_a0 context.Context, _a1 interface{}, _a2 interface{}, _a3 string, _a4 string) error {
	ret := _m.Called(_a0, _a1, _a2, _a3, _a4)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, interface{}, interface{}, string, string) error); ok {
		r0 = rf(_a0, _a1, _a2, _a3, _a4)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
