package mysql

import (
	"context"
	"testing"

	"github.com/joho/godotenv"

	"github.com/stretchr/testify/assert"
)

var ctx = context.Background()

func Connect() {
	godotenv.Load("../../.env")
	InitCon()
}

func TestModel_Create(t *testing.T) {
	Connect()

	var log Log
	log.Method = "GET"
	log.ApiUrl = "http://www.omdbapi.com/?apikey=faf7e5bb&s=Batman&page=1"
	log.RequestPayload = ""
	log.ResponsePayload = "{\"Title\":\"Batman: The Killing Joke\",\"Year\":\"2016\",\"Rated\":\"R\",\"Released\":\"25 Jul 2016\",\"Runtime\":\"76 min\",\"Genre\":\"Animation, Action, Crime\",\"Director\":\"Sam Liu\",\"Writer\":\"Brian Azzarello, Brian Bolland, Bob Kane\",\"Actors\":\"Kevin Conroy, Mark Hamill, Tara Strong\",\"Plot\":\"As Batman hunts for the escaped Joker, the Clown Prince of Crime attacks the Gordon family to prove a diabolical point mirroring his own fall into madness.\",\"Language\":\"English\",\"Country\":\"United States\",\"Awards\":\"1 win \\u0026 2 nominations\",\"Poster\":\"https://m.media-amazon.com/images/M/MV5BMTdjZTliODYtNWExMi00NjQ1LWIzN2MtN2Q5NTg5NTk3NzliL2ltYWdlXkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_SX300.jpg\",\"Ratings\":[{\"Source\":\"Internet Movie Database\",\"Value\":\"6.4/10\"},{\"Source\":\"Rotten Tomatoes\",\"Value\":\"39%\"}],\"Metascore\":\"N/A\",\"imdbRating\":\"6.4\",\"imdbVotes\":\"53,755\",\"imdbID\":\"tt4853102\",\"Type\":\"movie\",\"DVD\":\"02 Aug 2016\",\"BoxOffice\":\"$3,775,000\",\"Production\":\"N/A\",\"Website\":\"N/A\",\"Response\":\"True\",\"Error\":\"\"}"

	m := NewModel()
	err := m.Create(ctx, log, QueryCreateLog)
	assert.NoError(t, err)
}
