CREATE TABLE `logs` (
      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
      `request_payload` TEXT NOT NULL,
      `response_payload` TEXT NOT NULL,
      `api_url` varchar(255) NOT NULL,
      `method` varchar(255) NOT NULL,
      `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1041 DEFAULT CHARSET=utf8;