package mysql

const (
	QueryCreateLog = `
		INSERT INTO logs 
			(request_payload, response_payload, api_url, method)
		VALUES 
			(:request_payload, :response_payload, :api_url, :method);
	`
)
