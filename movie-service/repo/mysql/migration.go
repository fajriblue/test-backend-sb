package mysql

import (
	"fmt"
	"os"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/mysql"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	logger "github.com/sirupsen/logrus"
)

func RunMigrate() {
	logger.Println("⇨ Migration Start")
	urlConnection := fmt.Sprintf("%s://%s", os.Getenv("MYSQL_DB_DRIVER"), GetUrlConnection())
	m, err := migrate.New(
		"file://./repo/mysql/migration",
		urlConnection,
	)
	if err != nil {
		logger.Fatal("Migrate: ", err)
	}

	if err = m.Up(); err != nil {
		if err != migrate.ErrNoChange {
			logger.Fatal("Error Up Migrate: ", err)
		}
		logger.Printf("⇨ Mongodb Migration %s ... \n", err)
	} else {
		logger.Println("⇨ Migration Success")
	}
}
