package mysql

import (
	"context"
)

type IModel interface {
	Create(context.Context, interface{}, string) (err error)
}

type Model struct{}

func NewModel() *Model {
	return &Model{}
}

func (m *Model) Create(ctx context.Context, data interface{}, queryString string) (err error) {
	_, err = db.NamedExecContext(ctx, queryString, data)
	if err != nil {
		return
	}
	return
}
