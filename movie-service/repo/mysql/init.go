package mysql

import (
	"fmt"
	"os"

	logger "github.com/sirupsen/logrus"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

var db *sqlx.DB

func GetUrlConnection() string {
	return fmt.Sprintf("%s:%s@(%s:%s)/%s?charset=utf8&parseTime=true&loc=UTC",
		os.Getenv("MYSQL_DB_USER"),
		os.Getenv("MYSQL_DB_PASSWORD"),
		os.Getenv("MYSQL_DB_HOST"),
		os.Getenv("MYSQL_DB_PORT"),
		os.Getenv("MYSQL_DB_DATABASE"),
	)
}

// Connection is a function connection mysql with sqlx
func InitCon() {
	sqlxDb, err := sqlx.Connect(os.Getenv("MYSQL_DB_DRIVER"), GetUrlConnection())
	if err != nil {
		logger.Fatalln(err)
	}
	db = sqlxDb
}
