package mysql

type Log struct {
	ID              int64  `json:"id" db:"id"`
	RequestPayload  string `json:"request_payload" db:"request_payload"`
	ResponsePayload string `json:"response_payload" db:"response_payload"`
	ApiUrl          string `json:"api_url" db:"api_url"`
	Method          string `json:"method" db:"method"`
}
