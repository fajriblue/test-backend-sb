package action

import (
	"movie-service/builder"
	"movie-service/repo"
)

type Handler struct {
	iOmdb   repo.IOmdb
	builder *builder.Grpc
}

func NewHandler() *Handler {
	return &Handler{
		iOmdb:   repo.NewOmdb(),
		builder: builder.NewGrpc(),
	}
}
