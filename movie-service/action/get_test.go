package action

import (
	"errors"
	pb "movie-service/entity/movie"
	"movie-service/repo/rest"
	"movie-service/util"

	"github.com/stretchr/testify/require"

	"github.com/bxcodec/faker"
	"github.com/stretchr/testify/assert"
)

func (s *HandlerTestSuite) TestHandler_Get() {
	req := &pb.GetRequest{
		ImdbID: "tt4853102",
	}

	var resGet rest.Get
	err := faker.FakeData(&resGet)

	var resMovie pb.Movie
	util.Json.Unmarshal(resGet.Encode(), &resMovie)
	var resExpectation pb.GetResponse
	resExpectation.Movie = &resMovie

	s.oRepo.On("Get", ctx, req).Return(resGet, nil)

	resultActual, err := s.act.Get(ctx, req)
	assert.Nil(s.T(), err)
	assert.Equal(s.T(), resExpectation.Movie.ImdbID, resultActual.Movie.ImdbID)
}

func (s *HandlerTestSuite) TestHandler_GetFailed() {
	req := &pb.GetRequest{
		ImdbID: "tt4853102122",
	}

	var resGet rest.Get
	err := faker.FakeData(&resGet)

	errTest := errors.New("get error")
	s.oRepo.On("Get", ctx, req).Return(resGet, errTest)

	_, err = s.act.Get(ctx, req)

	require.Error(s.T(), err)
	require.EqualError(s.T(), errTest, err.Error())
}
