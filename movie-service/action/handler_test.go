package action

import (
	"context"
	"movie-service/builder"
	"movie-service/repo/mocks"
	"testing"

	"github.com/stretchr/testify/suite"
)

var ctx = context.Background()

type HandlerTestSuite struct {
	suite.Suite
	oRepo   *mocks.IOmdb
	act     *Handler
	builder *builder.Grpc
}

func (s *HandlerTestSuite) SetupTest() {
	s.oRepo = new(mocks.IOmdb)
	s.act = NewHandler()
	s.act.iOmdb = s.oRepo
	s.act.builder = builder.NewGrpc()
}

func TestHandlerTestSuite(t *testing.T) {
	suite.Run(t, new(HandlerTestSuite))
}

func (s *HandlerTestSuite) AfterTest(_, _ string) {
	s.oRepo.AssertExpectations(s.T())
}
