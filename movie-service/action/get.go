package action

import (
	"context"
	pb "movie-service/entity/movie"

	logger "github.com/sirupsen/logrus"
)

// list is a function get movie
func (h *Handler) Get(ctx context.Context, req *pb.GetRequest) (res *pb.GetResponse, err error) {
	logger.Infof("Request Get: %v", req)
	data, err := h.iOmdb.Get(ctx, req)
	if err != nil {
		logger.Errorf("Error Get: %v", err.Error())
		return
	}
	res = &pb.GetResponse{Movie: h.builder.BuildGet(data)}
	return
}
