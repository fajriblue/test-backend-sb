package action

import (
	"errors"
	pb "movie-service/entity/movie"
	"movie-service/repo/rest"
	"movie-service/util"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"github.com/bxcodec/faker"
)

func (s *HandlerTestSuite) TestHandler_List() {
	req := &pb.ListRequest{
		Search: "Batman",
		Page:   1,
	}

	var resList rest.List
	err := faker.FakeData(&resList)

	var resExpectation pb.ListResponse
	util.Json.Unmarshal(resList.Encode(), &resExpectation)

	s.oRepo.On("List", ctx, req).Return(resList, nil)

	resultActual, err := s.act.List(ctx, req)
	assert.Nil(s.T(), err)
	assert.Equal(s.T(), resExpectation.Search, resultActual.Search)
}

func (s *HandlerTestSuite) TestHandler_ListFailed() {
	req := &pb.ListRequest{
		Search: "v",
		Page:   1,
	}

	var resList rest.List
	err := faker.FakeData(&resList)

	errTest := errors.New("get error")
	s.oRepo.On("List", ctx, req).Return(resList, errTest)

	_, err = s.act.List(ctx, req)

	require.Error(s.T(), err)
	require.EqualError(s.T(), errTest, err.Error())
}
