package action

import (
	"context"
	pb "movie-service/entity/movie"
	"movie-service/util"

	logger "github.com/sirupsen/logrus"
)

// list is a function list movie
func (h *Handler) List(ctx context.Context, req *pb.ListRequest) (res *pb.ListResponse, err error) {
	logger.Infof("Request List: %v", req)
	data, err := h.iOmdb.List(ctx, req)
	if err != nil {
		logger.Errorf("Error List: %v", err.Error())
		return
	}
	// response to grpc
	res = &pb.ListResponse{
		Search:     h.builder.BuildList(data.Search),
		Pagination: h.builder.BuildPagination(req.Page, util.PageSize, util.StringToInt32(data.TotalResults)),
	}
	return
}
