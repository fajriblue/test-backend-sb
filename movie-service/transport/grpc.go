package transport

import (
	"movie-service/action"
	"net"
	"os"

	logger "github.com/sirupsen/logrus"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	pb "movie-service/entity/movie"
)

// RunGRCP is a function for run grpc
func (svc Server) RunGRCP() {
	lis, err := net.Listen("tcp", ":"+os.Getenv("GRPC_PORT"))
	if err != nil {
		logger.Fatalf("failed to listen: %v", err)
	} else {
		logger.Println("Connect GRPC to port " + os.Getenv("GRPC_PORT"))
	}
	s := grpc.NewServer()
	pb.RegisterMovieServiceServer(s, action.NewHandler())
	// Register reflection service on gRPC server.
	reflection.Register(s)
	if err = s.Serve(lis); err != nil {
		logger.Fatalf("failed to serve: %v", err)
	}
}
