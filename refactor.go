package test_backend_sb

import "strings"

const (
	bracketOpen = "("
	bracketClose = ")"
)

func subStringIndex(str, bracket string) int {
	return strings.Index(str,bracket)
}

func subRuneString(str string, idxOpen, idxClose int) string   {
	runes := []rune(str)
	return string(runes[idxOpen:idxClose])
}

func findFirstStringInBracket(str string) string {
	totalString := len(str)
	if totalString > 0 {
		indexFirstBracketFound := subStringIndex(str, bracketOpen)
		if indexFirstBracketFound >= 0 {
			wordsAfterFirstBracket := subRuneString(str, indexFirstBracketFound, totalString)
			indexClosingBracketFound := subStringIndex(wordsAfterFirstBracket, bracketClose)
			if indexClosingBracketFound >= 0 {
				return subRuneString(wordsAfterFirstBracket, 1, indexClosingBracketFound-1)
			}
		}
	}
	return ""
}