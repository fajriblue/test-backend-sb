# Online Test Backend Golang

## Testing
 - go test -v

## How
How to run Microservice :

1. copy .env.example to .env in movie-gateway and movie-service

2. command in terminal `make run`

## Postman API Documentation
[Movie API](https://documenter.getpostman.com/view/2341294/UVREjjBd)