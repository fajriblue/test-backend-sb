run:
	make -j 2 service gateway

service:
	cd movie-service && go run main.go

gateway:
	cd movie-gateway && go run main.go