package test_backend_sb

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_findAnagram(t *testing.T) {
	input := []string{"kita", "atik", "tika", "aku", "kia", "makan", "kua"}
	expected := [][]string{
		{"kita", "atik", "tika"},
		{"aku", "kua"},
		{"kia"},
		{"makan"},
	}
	assert.ElementsMatch(t, expected, findAnagrams(input))
}
