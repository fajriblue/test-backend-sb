package test_backend_sb

import "testing"

func Test_findFirstStringInBracket(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "Test 1",
			args: args{str: "as(fajri )9789"},
			want: "fajri",
		},
		{
			name: "Test 2",
			args: args{str: "learning (golang )"},
			want: "golang",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := findFirstStringInBracket(tt.args.str); got != tt.want {
				t.Errorf("findFirstStringInBracket() = %v, want %v", got, tt.want)
			}
		})
	}
}