package test_backend_sb

import (
	"sort"
)

type sortRunes []rune

func (s sortRunes) Less(i, j int) bool { return s[i] < s[j] }
func (s sortRunes) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
func (s sortRunes) Len() int           { return len(s) }

// sortRuneString is a function sort rune with string
func sortRuneString(s string) string {
	r := []rune(s)
	sort.Sort(sortRunes(r))
	return string(r)
}

// findAnagrams is a function find anagrams
func findAnagrams(input []string) [][]string {
	m := make(map[string][]string, 0)
	for _, v := range input {
		sortSlice := sortRuneString(v)
		m[sortSlice] = append(m[sortSlice], v)
	}

	var result [][]string
	for _, v := range m {
		result = append(result, v)
	}
	return result
}
